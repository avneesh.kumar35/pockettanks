﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class enemyMovement : MonoBehaviour {
    Transform player;
    //float enemySpeed=0.05f;
    Text LifeText;
    Text ScoreText;
    public AudioClip collisionsound;
    AudioSource audio;
    // Use this for initialization
    void Start () {
        audio = gameObject.AddComponent<AudioSource>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        LifeText = GameObject.FindWithTag("lifetext").GetComponent<Text>();
        ScoreText = GameObject.FindWithTag("scoretext").GetComponent<Text>();
        LifeText.text = "Life: " + PlayerController.life.ToString();
        ScoreText.text = "Enemies Killed: " + PlayerController.enemieskilled.ToString();
        
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 distancevec = player.position - this.transform.position;
        distancevec = distancevec.normalized;
        this.transform.position = this.transform.position + PlayerController.enemyspeed * distancevec;
        this.transform.eulerAngles = new Vector3(0.0f, 0.0f, 90+Mathf.Rad2Deg * Mathf.Atan2(distancevec.x,distancevec.y));
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            Destroy(this);
            Destroy(this.gameObject);
            PlayerController.life -= 20;
            LifeText.text = "Life " + PlayerController.life.ToString();

        }
        if (col.gameObject.tag == "bullet")
        {
            
            Destroy(this.gameObject);
            Destroy(col.gameObject);
            PlayerController.enemieskilled+=1;
            ScoreText.text = "Enemies Killed:" + PlayerController.enemieskilled.ToString();
            
        }
    }
}
