﻿using UnityEngine;
using System.Collections;

public class bullet : MonoBehaviour {

   Transform player;
	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").transform;
	}

    // Update is called once per frame
    void Update() {
        Vector3 distancevec = player.position - this.transform.position;
        float distance = distancevec.magnitude;
        if (distance > 50) {
            
            Destroy(gameObject);
        }
	   
	}
}
