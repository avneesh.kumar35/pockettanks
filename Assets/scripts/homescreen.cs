﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GoogleMobileAds.Api;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
public class homescreen : MonoBehaviour {
    public Button playButton;
    public Button exitButton;
    public Button leaderBoard;
    public Button achivemnets;
    public Button instructions;
    public Button share;
    public Text highscore;
  
    string subject = "Enemies Killed";
    string body = "I challenge you to beat my number of enemies killed:" + PlayerPrefs.GetInt("highscore", 0).ToString() + ". If you don't have this awsome game download it at https://play.google.com/store/apps/details?id=capricornus.app3.tanksattack";
    string adUnitId = "ca-app-pub-9408156079023384/4189716955";
    string bannerads = "ca-app-pub-9408156079023384/4050116156 ";
    InterstitialAd interstitial;
    AdRequest request;
    void Start()
    {
        PlayGamesPlatform.DebugLogEnabled = true;
        GooglePlayGames.PlayGamesPlatform.Activate();
        Social.localUser.Authenticate((bool success) => {
            // handle success or failure 			
            if (success)
            {

            }
        });
        Button play = playButton.GetComponent<Button>();
        play.onClick.AddListener(onPlayClick);
        Button exitbutton = exitButton.GetComponent<Button>();
        exitbutton.onClick.AddListener(exit);
        Button leaderboardbutton = exitButton.GetComponent<Button>();
        leaderboardbutton.onClick.AddListener(exit);
        Button instructionButton = instructions.GetComponent<Button>();
        instructionButton.onClick.AddListener(showinstructions);
        highscore.text = "High Score:" + PlayerPrefs.GetInt("highscore", 0).ToString();
        Button sharebutton = share.GetComponent<Button>();
        sharebutton.onClick.AddListener(shareScore);
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            if (PlayerPrefs.GetInt("died", 0) % 5 == 1) {
                interstitial = new InterstitialAd(adUnitId);
                // Create an empty ad request.
                request = new AdRequest.Builder().Build();
                // Load the interstitial with the request.
                interstitial.LoadAd(request);
                ShowInterstitial();
            }

            BannerView bannerView1 = new BannerView(bannerads, AdSize.Banner, AdPosition.Bottom);
            // Create an empty ad request.
            AdRequest request1 = new AdRequest.Builder().Build();
            // Load the banner with the request.
            bannerView1.LoadAd(request1);
            bannerView1.Show();


        }

    }
    void onPlayClick() {
        SceneManager.LoadScene(2);
    }

    void exit() {
        Application.Quit();
    }

    void showLeaderBoard() {
        if (Social.localUser.authenticated)
        {
            ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI("CgkI0pya7osBEAIQAA");
        }
    }

    void showAchievements()
    {
        if (Social.localUser.authenticated) {
            Social.ShowAchievementsUI();
        }

    }
    void showinstructions()
    {
        SceneManager.LoadScene(3);

    }
    public void shareScore()
    {
        StartCoroutine(ShareAndroidText());
    }

    IEnumerator ShareAndroidText()
    {
        yield return new WaitForEndOfFrame();
        //execute the below lines if being run on a Android device
    #if UNITY_ANDROID
            //Reference of AndroidJavaClass class for intent
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            //Reference of AndroidJavaObject class for intent
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
            //call setAction method of the Intent object created
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            //set the type of sharing that is happening
            intentObject.Call<AndroidJavaObject>("setType", "text/plain");
            //add data to be passed to the other activity i.e., the data to be sent
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);
            //intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TITLE"), "Text Sharing ");
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), body);
            //get the current activity
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            //start the activity by sending the intent data
            AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Share Via");
            currentActivity.Call("startActivity", jChooser);
    #endif
    }

    void ShowInterstitial()
    {


        while (!interstitial.IsLoaded())
        {
        }
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
        else
        {
            Debug.Log("Interstitial is not ready yet.");
        }
    }
}
