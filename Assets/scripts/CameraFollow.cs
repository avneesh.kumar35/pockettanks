﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
    public Transform playerpos;
	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update() {

        this.transform.position = new Vector3(playerpos.position.x, playerpos.position.y, -9);
        if (this.transform.position.y > 22) {
            this.transform.position = new Vector3(this.transform.position.x,22,-9);
        }
        if (this.transform.position.x < -30)
        {
            this.transform.position = new Vector3(-30, this.transform.position.y, -9);
        }
        if (this.transform.position.x > 30)
        {
            this.transform.position = new Vector3(30, this.transform.position.y, -9);
        }
        if (this.transform.position.y < -22)
        {
            this.transform.position = new Vector3(this.transform.position.x, -22, -9);
        }

    }
}
