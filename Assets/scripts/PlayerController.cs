﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class PlayerController : MonoBehaviour {

    private Rigidbody2D player;
    public Rigidbody2D bulletPrefab;
    public Rigidbody2D enemyPrefab;
    public Transform spawnpoint;
    public float bulletSpeed = 1000;
    static public int life;
    public static int enemieskilled;
    public static int enemyCount;
    public AudioClip firing;
    public static float enemyspeed;
    public float playerSpeed;
    AudioSource audio;
    bool isAndroid = false;
   
    // Use this for initialization
    void Start() {
        enemyspeed = 0.05f;
        life = 100;
        enemyCount = 0;
        enemieskilled = 0;
        playerSpeed = 0.1f;
        player = GetComponent<Rigidbody2D>();
        InvokeRepeating("createEnemy", 1.0f, 1.0f);
        InvokeRepeating("enemyspeedincrease", 5.0f, 5.0f);
        audio = gameObject.AddComponent<AudioSource>();
        
        if (Application.platform == RuntimePlatform.Android) {
            isAndroid = true;
            Input.gyro.enabled = true;
        }
    }

    // Update is called once per frame
    void Update() {
        if (life <= 0) {
            StoreHighScore(enemieskilled);
            PlayerPrefs.SetInt("died", PlayerPrefs.GetInt("died", 0)+1);
            Destroy(gameObject);
            SceneManager.LoadScene(1);
        }
        if (this.transform.position.y > 40) {
            this.transform.position = new Vector3(this.transform.position.x, 40, -9);
        }
        if (this.transform.position.y < -40)
        {
            this.transform.position = new Vector3(this.transform.position.x, -40, -9);
        }
        if (this.transform.position.x > 74)
        {
            this.transform.position = new Vector3(74, this.transform.position.y, -9);
        }
        if (this.transform.position.x < -74)
        {
            this.transform.position = new Vector3(-74, this.transform.position.y, -9);
        }

    }

    void FixedUpdate()
    {
        if (!isAndroid)
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");
            this.transform.eulerAngles = new Vector3(0.0f, 0.0f, 270 + Mathf.Rad2Deg * Mathf.Atan2(-horizontal, vertical));
            this.transform.position = new Vector3(this.transform.position.x + 0.1f * horizontal, this.transform.position.y + 0.1f * vertical, 0.0f);
            if (Input.GetMouseButtonUp(0))
            {
                Fire();
            }
        }
        else {
            //this.transform.eulerAngles = new Vector3(0.0f, 0.0f, 270 + Mathf.Rad2Deg * Mathf.Atan2(-horizontal, vertical));
            this.transform.Rotate(0.0f, 0.0f, (Mathf.Rad2Deg*-Input.gyro.rotationRateUnbiased.y)/2);
            if (Input.touchCount > 0) {
                Touch touch = Input.GetTouch(0);
                if (touch.position.x < Screen.width / 2)
                {
                    transform.position -= transform.right * playerSpeed;
                }
                else if (touch.position.x > Screen.width / 2 && touch.phase == TouchPhase.Began)
                {
                    Fire();
                }
                if (Input.touchCount > 1) {
                    touch = Input.GetTouch(1);
                    if (touch.position.x < Screen.width / 2)
                    {
                        transform.position -= transform.right * playerSpeed;
                    }
                    else if (touch.position.x > Screen.width / 2 && touch.phase == TouchPhase.Began)
                    {
                        Fire();
                    }
                }
            }
        }
        
    }

    void Fire() {
        audio.PlayOneShot(firing, 0.7F);
        Rigidbody2D bPrefab = Instantiate(bulletPrefab, spawnpoint.position, bulletPrefab.transform.rotation * transform.rotation) as Rigidbody2D;

        bPrefab.AddForce(-transform.right * bulletSpeed);
    }

    void createEnemy() {
        float x = (float)Random.Range(-64, 64);
        float y = (float)Random.Range(-30, 30);
        float val = Mathf.Pow((this.transform.position.x - x), 2)+ Mathf.Pow((this.transform.position.y - y), 2);
        if (val<100) {
            return;
        }
        if (enemyCount < 50) {
            Rigidbody2D ePrefab = Instantiate(enemyPrefab, new Vector3(x, y), bulletPrefab.transform.rotation * transform.rotation) as Rigidbody2D;
            ePrefab.transform.rotation = Quaternion.Euler(0,0,0);
        }
            
    }

    void enemyspeedincrease()
    {
        enemyspeed += 0.05f;
        playerSpeed += 0.05f;
        
    }


        void StoreHighScore(int Score)
    {
        int oldHighScore = PlayerPrefs.GetInt("highscore", 0);
        if (Score > oldHighScore)
        {
            if (Social.localUser.authenticated)
            {
                Social.ReportScore(Score, "CgkI0pya7osBEAIQAQ", (bool success) => {
                    // handle success or failure 					
                    if (success){
                        Debug.Log("Score posted successfully");
                    }
                    else {
                        Debug.Log("Not able to post score");
                    }
                });
                if (Score>=50){
                    Social.ReportProgress ("CgkI0pya7osBEAIQAQ", 100.0f,(bool success)=>{
                        if (success){
                            Debug.Log("Score posted successfully");
                        } 						else{
                            Debug.Log("Not able to post score");
                        }
                    });
                }
                if (Score>=200){
                    Social.ReportProgress ("CgkI0pya7osBEAIQAg", 100.0f,(bool success)=>{
                        if (success){
                            Debug.Log("Score posted successfully");
                        }
                        else {
                            Debug.Log("Not able to post score");
                        }
                    });
                }
                if (Score>=500){
                    Social.ReportProgress ("CgkI0pya7osBEAIQAw", 100.0f,(bool success)=>{
                        if (success){
                            Debug.Log("Score posted successfully");
                        }
                        else {
                            Debug.Log("Not able to post score");
                        }
                    });
                }
                if (Score>=1000){
                    Social.ReportProgress ("CgkI0pya7osBEAIQBA", 100.0f,(bool success)=>{
                        if (success){
                            Debug.Log("Score posted successfully");
                        }
                        else {
                            Debug.Log("Not able to post score");
                        }
                    });
                }
            }
            PlayerPrefs.SetInt ("highscore", Score);
        }
        if(PlayerPrefs.GetInt("died", 0)>=100)
        {
            Social.ReportProgress("CgkI0pya7osBEAIQBQ", 100.0f, (bool success) => {
                if (success)
                {
                    Debug.Log("Score posted successfully");
                }
                else
                {
                    Debug.Log("Not able to post score");
                }
            });

        }
    }
}
